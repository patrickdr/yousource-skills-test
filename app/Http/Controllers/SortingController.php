<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class SortingController extends Controller
{
    //
    public function getSort(Request $request) {        
        $data = $request->all();
        $result = null;
        if($data) {
            if(function_exists($data['sort_type'])) {
                $result = call_user_func($data['sort_type'], $data['string']);      
                $result = implode(array_filter($result));
            }
        }
        return view('sorting', compact('result'));
    }
}
