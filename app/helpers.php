<?php

if(!function_exists('quick_sort')) {

    function quick_sort($string) {
        $array = str_split($string);        
        $length = count($array);        
        if($length <= 1){
            return $array;
        } else {
                    
            $pivot = $array[0];
            $left = $right = array();
            for($i = 1; $i < count($array); $i++) {
                if($array[$i] < $pivot) {
                    $left[] = $array[$i];
                } else {
                    $right[] = $array[$i];
                }
            }
            return array_filter(array_merge(quick_sort(implode('', $left)), array($pivot), quick_sort(implode('', $right))));
        }
    }
    
}

if(!function_exists('bubble_sort')) {
    function bubble_sort($string) {
        $array = str_split($string);
        $length = count($array);
        $counter = 0;
        while($counter < $length) {            
            for($x = 0; $x < $length-1; $x++) {
                $value = $array[$x];
                $temp1 = $value;
                for($i = $x+1; $i < $length; $i++) {
                    $temp2 = $array[$i];
                    if($temp1 > $temp2) {                    
                        $array[$i-1] = $temp2;
                        $array[$i] = $temp1;
                    } else {
                        continue 2;
                    }
                }                
            }
            $counter++;
        }
        return $array;
    }
}

if(!function_exists('merge_sort')) {
    function merge_sort($string) {
        $array = str_split($string);
        $length = count($array);  
        
        if($length == 1) {
            return $array;
        }
 
        $mid = count($array) / 2;
        $left = array_slice($array, 0, $mid);
        $right = array_slice($array, $mid);
    
        $left = merge_sort(implode('', array_filter($left)));
        $right = merge_sort(implode('', array_filter($right)));
        
        return merge($left, $right);        
    }
}

if(!function_exists('merge')) {
    function merge($left, $right) {
        $result=array();
        $leftIndex=0;
        $rightIndex=0;
     
        while($leftIndex < count($left) && $rightIndex < count($right)) {
            if($left[$leftIndex] > $right[$rightIndex]) {     
                $result[] = $right[$rightIndex];
                $rightIndex++;
            } else {
                $result[] = $left[$leftIndex];
                $leftIndex++;
            }
        }
        while($leftIndex<count($left)) {
            $result[] = $left[$leftIndex];
            $leftIndex++;
        }
        while($rightIndex<count($right)) {
            $result[] = $right[$rightIndex];
            $rightIndex++;
        }
        return $result;
    }    
}
